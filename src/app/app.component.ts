import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
} from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "form-testing";
  form: FormGroup;
  formValid: boolean = false;
  @Output() login = new EventEmitter<User>();
  datasource = new MatTableDataSource<Students>(DUMMY_DATA);
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayColumns: string[] = [
    "name",
    "family_name",
    "phone_number",
    "email_address",
  ];

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.datasource.paginator = this.paginator;
    this.form = this.fb.group({
      email: [
        "",
        [
          Validators.required,
          Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}"),
        ],
      ],
      password: ["", [Validators.required, Validators.minLength(8)]],
    });
  }

  ngAfterViewInit() {
    this.datasource.sort = this.sort;
    this.datasource.paginator = this.paginator;
  }

  onSubmitClick() {
    if (this.form.valid) {
      this.login.emit(
        new User(this.form.value.email, this.form.value.password)
      );
    }
  }
}

export class User {
  email: string;
  password: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}

export interface Students {
  name: string;
  f_name: string;
  phone: string;
  email: string;
}

const DUMMY_DATA: Students[] = [
  { name: "Mahsa", f_name: "Rohani", phone: "876665", email: "aaa@yyy.com" },
  { name: "Amir", f_name: "Rohani", phone: "098765", email: "bbb@uuu.com" },
  { name: "Parham", f_name: "Rohani", phone: "0975644", email: "ccc@ccc.com" },
  { name: "Parsa", f_name: "Rohani", phone: "487790", email: "ooo@www.com" },
  {
    name: "Taranom",
    f_name: "Ghaderi",
    phone: "9045678",
    email: "sss@qqqq.com",
  },
];
