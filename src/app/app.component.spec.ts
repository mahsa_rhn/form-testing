import { TestBed, async } from "@angular/core/testing";
import { BrowserModule, By } from "@angular/platform-browser";
import { AppComponent, Students } from "./app.component";
import { MatSort, MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatPaginator, MatPaginatorModule } from "@angular/material/paginator";

describe("AppComponent", () => {
  let fixture;
  let app;
  let htmlel: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        MatSortModule,
        MatTableModule,
        MatPaginatorModule,
      ],
    })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AppComponent);
        app = fixture.componentInstance;
        app.ngOnInit();
      });
  }));

  let inputEmail: string;
  let inputPassword: string;

  function updateForm(userEmail, userPassword) {
    app.form.controls["email"].setValue(userEmail);
    app.form.controls["password"].setValue(userPassword);
    inputEmail = userEmail;
    inputPassword = userPassword;
    return { inputEmail, inputPassword };
  }

  it("should create the app", () => {
    expect(app).toBeTruthy();
  });

  it("form should be invalid", async () => {
    updateForm("", "");
    expect(app.form.invalid).toBeTruthy();
  });

  it("email field validity", () => {
    let errors = {};
    let email = app.form.controls["email"];
    expect(email.valid).toBeFalsy();

    // Email field is required
    errors = email.errors || {};
    expect(errors["required"]).toBeTruthy();

    // Set email to invalid value
    email.setValue("test@mmm");
    errors = email.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["pattern"]).toBeTruthy();

    // Set email to valid value
    email.setValue("test@example.com");
    errors = email.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["pattern"]).toBeFalsy();
  });

  it("password field validity", () => {
    let errors = {};
    let password = app.form.controls["password"];

    // Password field is required
    errors = password.errors || {};
    expect(errors["required"]).toBeTruthy();

    // Set Password to invalid value
    password.setValue("123456");
    errors = password.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["minlength"]).toBeTruthy();

    // Set Password to valid value
    password.setValue("123456789");
    errors = password.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["minlength"]).toBeFalsy();
  });

  it("on submit click call", async () => {
    spyOn(app, "onSubmitClick");
    fixture.detectChanges();
    htmlel = fixture.debugElement.query(By.css("button")).nativeElement;
    htmlel.click();

    expect(app.onSubmitClick).toHaveBeenCalledTimes(1);
  });

  it(`on submit click'`, () => {
    updateForm("eee@yahoo.com", "mahsa1234");
    expect(app.form.invalid).toBeFalsy();

    let user;

    app.login.subscribe((value) => (user = value));
    app.onSubmitClick();
    expect(user.email).toEqual(inputEmail);
    expect(user.password).toEqual(inputPassword);
  });

  it("should populate datasource", async () => {
    expect(app.datasource).not.toBeNull();
  });

  it("initially sets up sorting", () => {
    fixture.detectChanges();

    const sort = app.datasource.sort;
    expect(sort).toBeInstanceOf(MatSort);
  });

  it("should be able to render a table correctly with native elements", () => {
    fixture.detectChanges();

    const tableElement = fixture.nativeElement.querySelector("table");
    const data = fixture.componentInstance.datasource!.data;
    const tableHeaders = Array.from(
      tableElement.getElementsByClassName("mat-header-cell")
    );
    let column = [];
    tableHeaders.forEach((col) => column.push(col["innerText"]));
    let checker = (arr, target) => target.every((v) => arr.includes(v));
    expect(
      checker(column, ["Name", "Family name", "Phone number", "Email address"])
    );
  });

  it("initially sets up paginator and work fine", async () => {
    fixture.detectChanges();
    const page = app.datasource.paginator;
    let i = 1;
    expect(page).toBeInstanceOf(MatPaginator);
    while (app.datasource.paginator.hasNextPage()) {
      i++;
      app.datasource.paginator.nextPage();
    }
    expect(i).toEqual(1);
  });
});
